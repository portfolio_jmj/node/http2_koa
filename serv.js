const http2 = require('http2');
const fs = require('fs');

const Koa = require('koa');
const Router = require('koa-router');
const Body = require('koa-body');

const router = Router();
const app = new Koa();
app.use(Body());


const port = 8443;


const server = http2.createSecureServer({
  allowHTTP1: true,
  key: fs.readFileSync('localhost-privkey.pem'),
  cert: fs.readFileSync('localhost-cert.pem')
}, app.callback());
server.on('error', (err) => console.error(err));

/*
 full node
server.on('stream', (stream, headers) => {
  // stream is a Duplex
  stream.respond({
    'content-type': 'text/html',
    ':status': 200
  });
  stream.end('<h1>Hello World</h1>');
});
*/

router.get('/test', ctx => {

  ctx.response.body = 'Salut';
});


app.use(router.routes());



console.log('Server listen to : ' + port);
server.listen(port);